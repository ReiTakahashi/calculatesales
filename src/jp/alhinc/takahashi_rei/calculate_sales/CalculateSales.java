package jp.alhinc.takahashi_rei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) throws IOException {


		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> uriagemap = new HashMap<String, Long>();
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(!shitenkakunin(args[0],"branch.lst",branchmap,uriagemap)) {
			return;
		}

		File file = new File(args[0]);
		File files[] = file.listFiles();
		ArrayList<File> uriage = new ArrayList<File>() ;//for用
		for (int i=0; i<files.length; i++) {
			String str =files[i].getName();

			if(str.matches("[0-9]{8}"+ ".rcd" ) && (files[i].isFile())) {
				uriage.add(files[i]);
			}
	    }

		for(int i = 0 ; i < uriage.size()-1; i++) {
        	String Henkanmaestr = uriage.get(i).getName().substring(0, 8);
        	String Henkanmaestr1 = uriage.get(i+1).getName().substring(0, 8);
        	int Henkanint = Integer.parseInt(Henkanmaestr);
        	int Henkanint1 = Integer.parseInt(Henkanmaestr1);
        	if(Henkanint1 - Henkanint!=1) {
        		System.out.println("売上ファイル名が連番になっていません");
        		return;
        	}
        }


		FileReader fr1 = null;
		BufferedReader brf  = null;

		try{
			for(int j = 0 ; j < uriage.size(); j++) {
				File file1 = new File(args[0],uriage.get(j).getName());
				fr1 = new FileReader(file1);
				brf = new BufferedReader(fr1);
				String line1  ;
				List<String> uriageList = new ArrayList<String>();
				while((line1 = brf.readLine()) !=null) {
					uriageList.add(line1);
				}
				if(uriageList.size() != 2 ) {
					System.out.println(uriage.get(j).getName() + "のフォーマットが不正です");
					return;
				}
				if(!branchmap.containsKey(uriageList.get(0))) {
					System.out.println(uriage.get(j).getName() + "の支店コードが不正です");
					return;
				}
				if (!uriageList.get(1).matches("^[0-9]+$") ){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long amount = uriagemap.get(uriageList.get(0)) + Long.parseLong( uriageList.get(1));
				if(amount  > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			uriagemap.put(uriageList.get(0), amount);
			}
		}
		catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}finally {
			if(brf != null && fr1 != null) {
				try {
					brf.close();
					fr1.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		if(!filekakidashi(args[0],"branch.out",branchmap,uriagemap)) {
			return;
		}

	}


	public static boolean shitenkakunin(String directory1, String filename1 , HashMap<String, String> branchmap,
			HashMap<String, Long> uriagemap) {
		BufferedReader br  = null;
		try {

			File file = new File(directory1,  filename1);
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) !=null) {
				String[] shiten = line.split(",");
				if(shiten.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!shiten[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				branchmap.put(shiten[0], shiten[1]);
				uriagemap.put(shiten[0],0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		return true;
	}

	public static boolean filekakidashi(String directory,String filename,HashMap<String,String>branchmap,HashMap<String,Long>uriagemap) {
		BufferedWriter bw = null;

		try {
			File f = new File(directory,filename);
			bw = new BufferedWriter(new FileWriter(f));
			for (String key : branchmap.keySet()) {
				bw.write(key + ","+ branchmap.get(key) +","+ uriagemap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}return true;

	}
}


